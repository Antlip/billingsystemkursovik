﻿using BillingSystem.Models.Interfaces;
using BillingSystem.Models.Interfaces.IRepository;
using BillingSystem.Models.Interfaces.IService;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace BillingSystem.Services
{
    public abstract class BaseService<TModel> : IService<TModel, Guid>
        where TModel : class, IEntity<Guid>
    {
        protected readonly IRepository<TModel, Guid> _repository;
        //protected readonly IMapper _mapper;
        public BaseService(IRepository<TModel, Guid> repository)
        {
            _repository = repository;
            //_mapper = mapper;
        }

        public virtual async Task<TModel> AddAsync(TModel modelDto, CancellationToken cancellationToken = default)
        {
            if (modelDto is null)
                throw new ArgumentNullException();

            //var model = _mapper.Map<TModel>(modelDto);

            await _repository.AddAsync(modelDto, cancellationToken);
            return modelDto;
        }

        public virtual async Task<IEnumerable<TModel>> GetAllAsync(CancellationToken cancellationToken = default)
        {
            var listModelDto = await _repository.GetAllAsync(cancellationToken);

            return listModelDto is null ? throw new ArgumentException() :listModelDto;
        }

        public virtual async Task<TModel> GetByIdAsync(Guid id, CancellationToken cancellationToken = default)
        {
            if (id == Guid.Empty)
                throw new ArgumentNullException();

            var modelDto = await _repository.GetByIdAsync(id, cancellationToken);

            return modelDto is null ? throw new ArgumentException() : modelDto;
        }

        public virtual async Task<TModel> RemoveAsync(Guid id, CancellationToken cancellationToken = default)
        {
            if (id == Guid.Empty)
                throw new ArgumentNullException();

            var model = await _repository.RemoveAsync(id, cancellationToken);

            if (model is null)
                throw new ArgumentException();
            


            return model;
        }

        public virtual async Task<TModel> UpdateAsync(Guid id, TModel modelDto, CancellationToken cancellationToken = default)
        {
            if (id != modelDto.Id)
                throw new ArgumentNullException();
            
            await _repository.UpdateAsync(modelDto.Id, modelDto, cancellationToken);

            return modelDto;
        }
    }
}