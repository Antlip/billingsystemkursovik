﻿using BillingSystem.Models.DbModels;
using BillingSystem.Models.Interfaces.IRepository;
using BillingSystem.Models.Interfaces.IService;

namespace BillingSystem.Services
{
    public class ProductService : BaseService<Product>, IProductService
    {
        public ProductService(IProductRepository productRepository)
            :base(productRepository)
        {

        }
    }
}
