﻿using BillingSystem.Models.DbModels;
using BillingSystem.Models.Interfaces.IRepository;
using BillingSystem.Models.Interfaces.IService;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace BillingSystem.Services
{
    public class ClientService : BaseService<BillingClient>, IClientService
    {
        private readonly IProductService _productService;
        public ClientService(IClientRepository clientRepository, IProductService productService)
            :base(clientRepository)
        {
            _productService = productService;
        }

        public async Task AddInterestsGoodsAsync(Guid clientId, List<string> interestingGoods, CancellationToken cancellationToken = default)
        {
            if (Guid.Empty == clientId || interestingGoods is null)
                throw new ArgumentNullException();

            var client = await _repository.GetByIdAsync(clientId);
            if(client is null)
                throw new ArgumentNullException();

            client.TypesProducts.AddRange(interestingGoods);

            await _repository.UpdateAsync(clientId, client);
            if (client is null)
                throw new ArgumentNullException();
            await _repository.UpdateAsync(clientId, client);
        }

        public async Task<List<Product>> GetAllProduct(CancellationToken cancellationToken = default)
        {
            return (List<Product>)await _productService.GetAllAsync(cancellationToken);
        }

        public async Task SuggestProductToClientAsync(Guid clientId, List<Guid> productIds, CancellationToken cancellationToken = default)
        {
            if (Guid.Empty == clientId || productIds is null)
                throw new ArgumentNullException();

            var client = await _repository.GetByIdAsync(clientId);
            if (client is null)
                throw new ArgumentNullException();

            foreach (var prodId in productIds)
            {
                if (Guid.Empty == prodId)
                    throw new ArgumentNullException();

                var product = await _productService.GetByIdAsync(prodId);
                if (!(product is null))
                {
                    client.SuggestedProducts.Add(product);
                }

            }
            await _repository.UpdateAsync(clientId, client);
        }
       
    }
}
