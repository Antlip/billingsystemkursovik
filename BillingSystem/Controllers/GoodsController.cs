﻿using BillingSystem.Models.DbModels;
using BillingSystem.Models.Interfaces.IService;
using BillingSystem.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Diagnostics;
using System.Threading.Tasks;

namespace BillingSystem.Controllers
{
     [Authorize(Roles = "Admin")]
    public class GoodsController : Controller
    {
        private readonly IProductService _productService;
        public GoodsController(IProductService productService)
        {
            _productService = productService;
        }
        public async Task<IActionResult> Index()
        {
            try
            {
                var products = await _productService.GetAllAsync();
                return View(products);

            }
            catch (Exception ex)
            {
                ErrorViewModel errorViewModel = new ErrorViewModel
                {
                    RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier,
                    Messge = ex.Message
                };

                return RedirectToAction("Error", errorViewModel);
            }
        }
        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> Create(Product product)
        {
            
                try
                {
                    await _productService.AddAsync(product);
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    ErrorViewModel errorViewModel = new ErrorViewModel
                    {
                        RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier,
                        Messge = ex.Message
                    };

                    return RedirectToAction("Error", errorViewModel);
                }
            
        }
        
        [HttpGet]
        public async Task<IActionResult> EditAsync(Guid Id)
        {
            try
            {
                var product = await _productService.GetByIdAsync(Id);
                return View(product);
            }
            catch (Exception ex)
            {
                ErrorViewModel errorViewModel = new ErrorViewModel
                {
                    RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier,
                    Messge = ex.Message
                };

                return RedirectToAction("Error", "Home", errorViewModel);
            }
        }
        [HttpPost]
        public async Task<IActionResult> EditAsync(Product product)
        {
            try
            {
                if (product is null)
                    throw new ArgumentNullException();
                var updatedProduct = await _productService.UpdateAsync(product.Id, product);
                if (updatedProduct is null)
                    throw new ArgumentNullException();
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                ErrorViewModel errorViewModel = new ErrorViewModel
                {
                    RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier,
                    Messge = ex.Message
                };

                return RedirectToAction("Error", errorViewModel);
            }
        }
       
        public async Task<IActionResult> DeleteAsync(Guid Id)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var prod = await _productService.RemoveAsync(Id);
                    if (prod is null)
                        throw new ArgumentNullException();
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    ErrorViewModel errorViewModel = new ErrorViewModel
                    {
                        RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier,
                        Messge = ex.Message
                    };

                    return RedirectToAction("Error", errorViewModel);
                }
            }
            return View();
        }
    }
}
