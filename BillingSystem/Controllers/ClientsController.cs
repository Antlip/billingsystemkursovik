﻿using BillingSystem.Models.DbModels;
using BillingSystem.Models.Interfaces.IService;
using BillingSystem.ViewModels;
using BillingSystem.ViewModels.Clients;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace BillingSystem.Controllers
{
     [Authorize(Roles = "Admin")]
    public class ClientsController : Controller
    {
        private readonly IClientService _clientService;
        private readonly UserManager<ApplicationUser> _userManager;
        public ClientsController(IClientService clientService, UserManager<ApplicationUser> userManager)
        {
            _clientService = clientService;
            _userManager = userManager;
        }
        public async Task<IActionResult> IndexAsync()
        {
            try
            {
                var clients = await _clientService.GetAllAsync();
                return View(clients);

            }
            catch (Exception ex)
            {
                ErrorViewModel errorViewModel = new ErrorViewModel
                {
                    RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier,
                    Messge = ex.Message
                };

                return RedirectToAction("Error", errorViewModel);
            }
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> Create(BillingClient client)
        {

            try
            {
                await _clientService.AddAsync(client);
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                ErrorViewModel errorViewModel = new ErrorViewModel
                {
                    RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier,
                    Messge = ex.Message
                };

                return RedirectToAction("Error", errorViewModel);
            }

        }

        [HttpGet]
        public async Task<IActionResult> EditAsync(Guid Id)
        {
            try
            {
                var client = await _clientService.GetByIdAsync(Id);
                return View(client);
            }
            catch (Exception ex)
            {
                ErrorViewModel errorViewModel = new ErrorViewModel
                {
                    RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier,
                    Messge = ex.Message
                };

                return RedirectToAction("Error", "Home", errorViewModel);
            }
        }
        [HttpPost]
        public async Task<IActionResult> EditAsync(BillingClient client)
        {
            try
            {
                if (client is null)
                    throw new ArgumentNullException();
                var updatedProduct = await _clientService.UpdateAsync(client.Id, client);
                if (updatedProduct is null)
                    throw new ArgumentNullException();
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                ErrorViewModel errorViewModel = new ErrorViewModel
                {
                    RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier,
                    Messge = ex.Message
                };

                return RedirectToAction("Error", errorViewModel);
            }
        }

        public async Task<IActionResult> DeleteAsync(Guid Id)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var client = await _clientService.RemoveAsync(Id);
                    if (client is null)
                        throw new ArgumentNullException();
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    ErrorViewModel errorViewModel = new ErrorViewModel
                    {
                        RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier,
                        Messge = ex.Message
                    };

                    return RedirectToAction("Error", errorViewModel);
                }
            }
            return View();
        }

        public async Task<IActionResult> DetailsAsync(Guid Id)
        {

            try
            {
                var client = await _clientService.GetByIdAsync(Id);
                if (client is null)
                    throw new ArgumentNullException();
                return View(client);
            }
            catch (Exception ex)
            {
                ErrorViewModel errorViewModel = new ErrorViewModel
                {
                    RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier,
                    Messge = ex.Message
                };

                return RedirectToAction("Error", errorViewModel);
            }

        }


        [HttpGet]
        public async Task<IActionResult> SuggestProductToClientAsync(Guid Id)
        {
            try
            {
                var model = new List<SuggestProductClientViewModel>();
                var client = await _clientService.GetByIdAsync(Id);
                client.ManagerId = _userManager.GetUserId(User);
                if (client == null)
                {
                    ViewBag.ErrorMessage = $"client with Id = {Id} not fount";
                    return View("Error/NotFound");
                }
                var products = await _clientService.GetAllProduct();

                foreach (var item in products)
                {
                    model.Add(new SuggestProductClientViewModel
                    {
                        IsChoosen = false,
                        ProductId = item.Id,
                        ProductName = item.ProductName
                    });
                }
                ViewBag.clientId = client.Id;
                return View(model);
            }
            catch (Exception ex)
            {
                ErrorViewModel errorViewModel = new ErrorViewModel
                {
                    RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier,
                    Messge = ex.Message
                };

                return RedirectToAction("Error", errorViewModel);
            }
        }
        [HttpPost]
        public async Task<IActionResult> SuggestProductToClientAsync(List<SuggestProductClientViewModel> model, Guid Id)
        {
            try
            {

                List<Guid> prodIds = new List<Guid>();
                foreach (var item in model)
                {
                    if (item.IsChoosen)
                    {
                        prodIds.Add(item.ProductId);
                    }
                }
                await _clientService.SuggestProductToClientAsync(Id, prodIds);
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                ErrorViewModel errorViewModel = new ErrorViewModel
                {
                    RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier,
                    Messge = ex.Message
                };

                return RedirectToAction("Error", errorViewModel);
            }
        }

        [HttpGet]
        public async Task<IActionResult> PrefferedProducts(Guid Id)
        {
            try
            {

                var model = new PrefferedProducts();
                var client = await _clientService.GetByIdAsync(Id);
                if (client == null)
                {
                    ViewBag.ErrorMessage = $"client with Id = {Id} not fount";
                    return View("Error/NotFound");
                }

                ViewBag.clientId = client.Id;
                return View(model);
            }
            catch (Exception ex)
            {
                ErrorViewModel errorViewModel = new ErrorViewModel
                {
                    RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier,
                    Messge = ex.Message
                };

                return RedirectToAction("Error", errorViewModel);
            }
        }
        [HttpPost]
        public async Task<IActionResult> PrefferedProducts(PrefferedProducts model, Guid Id)
        {
            try
            {
                if (model is null)
                    throw new ArgumentNullException();
                var prodList = model.ProductNames.Split(";");
                await _clientService.AddInterestsGoodsAsync(Id, prodList.ToList());
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                ErrorViewModel errorViewModel = new ErrorViewModel
                {
                    RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier,
                    Messge = ex.Message
                };

                return RedirectToAction("Error", errorViewModel);
            }
        }
        [HttpGet]
        public async Task<IActionResult> AddTaskAsync(string Id)
        {
            try
            {
                var model = new List<AddTaskUserViewModel>();
                var clients = await _clientService.GetAllAsync();
                foreach (var c in clients)
                {
                    if (c.SuggestedProducts.Count == 0)
                    {
                        model.Add(new AddTaskUserViewModel
                        {
                            ClientId = c.Id,
                            ClientName = c.FirstName + c.LastName,
                            IsChoosen = false
                        });

                    }
                }
                ViewBag.userId = Id;
                return View(model);
            }
            catch (Exception ex)
            {
                ErrorViewModel errorViewModel = new ErrorViewModel
                {
                    RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier,
                    Messge = ex.Message
                };
                return RedirectToAction("Error", errorViewModel);
            }
        }
        [HttpPost]
        public async Task<IActionResult> AddTaskAsync(List<AddTaskUserViewModel> model, string Id)
        {
            try
            {
                if (!(model is null))
                {
                    var clientsIds = new List<Guid>();
                    foreach (var c in model)
                    {
                        if (c.ClientId != Guid.Empty)
                        {
                            clientsIds.Add(c.ClientId);
                        }
                    }
                    await _clientService.SetManagerToClient(clientsIds, Id);
                }
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                ErrorViewModel errorViewModel = new ErrorViewModel
                {
                    RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier,
                    Messge = ex.Message
                };
                return RedirectToAction("Error", errorViewModel);
            }
        }
    }
}
