﻿using System;

namespace BillingSystem.ViewModels.AdminViewModels
{
    public class UserRoleViewModel
    {
        public Guid UserId { get; set; }
        public string UserName { get; set; }
        public bool IsSelected { get; set; }
    }
}
