﻿namespace BillingSystem.ViewModels.AdminViewModels
{
    public class EditRolesInUser
    {
        public string RoleId { get; set; }
        public string RoleName { get; set; }
        public bool IsSelected { get; set; }
    }
}
