﻿using System.ComponentModel.DataAnnotations;

namespace BillingSystem.ViewModels.AdminViewModels
{
    public class CreateRoleViewModel
    {
        [Required]
        public string RoleManager { get; set; }
    }
}
