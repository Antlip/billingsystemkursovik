﻿using System;

namespace BillingSystem.ViewModels.Clients
{
    public class SuggestProductClientViewModel
    {
        public Guid ProductId{ get; set; }
        public string ProductName { get; set; }
        public bool IsChoosen { get; set; }

    }
}
