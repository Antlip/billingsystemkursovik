﻿using System;

namespace BillingSystem.ViewModels.Clients
{
    public class AddTaskUserViewModel
    {
        public Guid ClientId { get; set; }
        public string  ClientName { get; set; }
        public bool IsChoosen {get;set;}
    }
}
