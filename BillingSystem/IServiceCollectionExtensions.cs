﻿using BillingSystem.DataAccess;
using BillingSystem.Models.DbModels;
using BillingSystem.Models.Interfaces.IRepository;
using BillingSystem.Models.Interfaces.IService;
using BillingSystem.Services;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace BillingSystem
{
    public static class IServiceCollectionExtensions
    {
        public static void AddIdentityConfiguration(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddIdentity<ApplicationUser, ApplicationRole>()
                .AddMongoDbStores<ApplicationUser, ApplicationRole, Guid>(
                    configuration["MongoDbSettings:ConnectionString"],
                    configuration["MongoDbSettings:DatabaseName"])
                .AddDefaultTokenProviders();
        }

        public static void AddRepositories(this IServiceCollection services)
        {
            services.AddTransient<IClientRepository, ClientRepository>();
            services.AddTransient<IProductRepository, ProductRepository>();
        }

        public static void AddServices(this IServiceCollection services)
        {
            services.AddTransient<IClientService, ClientService>();
            services.AddTransient<IProductService, ProductService>();
        }
    }
}
