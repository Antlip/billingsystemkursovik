﻿using BillingSystem.Models.DbModels;
using BillingSystem.Models.Interfaces.IRepository;
using BillingSystem.Models.SettingsClass;
using Microsoft.Extensions.Options;

namespace BillingSystem.DataAccess
{
    public class ClientRepository : BaseRepository<BillingClient>, IClientRepository
    {
        public ClientRepository(IOptions<MongoDBSettings> mongoDbSettings) 
            : base(mongoDbSettings)
        {

        }
    }
}
