﻿using BillingSystem.Models.DbModels;
using BillingSystem.Models.Interfaces.IRepository;
using BillingSystem.Models.SettingsClass;
using Microsoft.Extensions.Options;

namespace BillingSystem.DataAccess
{
    public class ProductRepository : BaseRepository<Product>, IProductRepository
    {
        public ProductRepository(IOptions<MongoDBSettings> mongoDbSettings)
            : base(mongoDbSettings)
        {

        }
    }
}
