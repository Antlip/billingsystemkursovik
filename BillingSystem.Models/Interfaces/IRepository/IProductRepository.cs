﻿using BillingSystem.Models.DbModels;
using System;

namespace BillingSystem.Models.Interfaces.IRepository
{
    public interface IProductRepository : IRepository<Product, Guid>
    {
    }
}
