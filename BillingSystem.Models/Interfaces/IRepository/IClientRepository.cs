﻿using BillingSystem.Models.DbModels;
using System;

namespace BillingSystem.Models.Interfaces.IRepository
{
    public interface IClientRepository : IRepository<BillingClient, Guid>
    {
    }
}
