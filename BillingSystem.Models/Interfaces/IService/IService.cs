using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace BillingSystem.Models.Interfaces.IService
{
    public interface IService<TModel, TId>
        where TModel : IEntity<TId>
    {
        Task<TModel> AddAsync(TModel obj, CancellationToken cancellationToken = default);
        Task<IEnumerable<TModel>> GetAllAsync(CancellationToken cancellationToken = default);
        Task<TModel> GetByIdAsync(TId id, CancellationToken cancellationToken = default);
        Task<TModel> UpdateAsync(TId id, TModel obj, CancellationToken cancellationToken = default);
        
        Task<TModel> RemoveAsync(TId id, CancellationToken cancellationToken = default);
    }
}