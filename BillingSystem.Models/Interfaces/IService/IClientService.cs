﻿using BillingSystem.Models.DbModels;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace BillingSystem.Models.Interfaces.IService
{
    public interface IClientService : IService<BillingClient, Guid>
    {
        Task SuggestProductToClientAsync(Guid clientId, List<Guid> productIds, CancellationToken cancellationToken = default);

        Task AddInterestsGoodsAsync(Guid clientId, List<string> interestingGoods, CancellationToken cancellationToken = default);

        Task<List<Product>> GetAllProduct(CancellationToken cancellationToken = default);

    }
}
