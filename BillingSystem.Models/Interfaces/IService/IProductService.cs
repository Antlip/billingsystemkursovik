﻿using BillingSystem.Models.DbModels;
using System;

namespace BillingSystem.Models.Interfaces.IService
{
    public interface IProductService : IService<Product, Guid>
    {
    }
}
