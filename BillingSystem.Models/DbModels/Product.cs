﻿using BillingSystem.Models.Interfaces;
using System;

namespace BillingSystem.Models.DbModels
{
    public class Product : IEntity<Guid>
    {
        public Guid Id { get; set; }
        public string ProductName { get; set; }
        public int Value { get; set; }
        public int Count { get; set; }

        public string Description { get; set; }
    }
}
