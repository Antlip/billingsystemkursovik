﻿using AspNetCore.Identity.MongoDbCore.Models;

namespace BillingSystem.Models.DbModels
{

    public class ApplicationRole : MongoIdentityRole
    {
       
        public ApplicationRole() : base()
        {
        }

        public ApplicationRole(string roleName) : base(roleName)
        {

        }
    }
}