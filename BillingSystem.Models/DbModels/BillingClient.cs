using BillingSystem.Models.Interfaces;
using System;
using System.Collections.Generic;

namespace BillingSystem.Models.DbModels
{
    public class BillingClient : IEntity<Guid>
    {
        public BillingClient()
        {
            SuggestedProducts = new List<Product>();
            TypesProducts = new List<string>();
        }
        public Guid Id { get ; set ; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }

        public string ManagerId { get; set; }
        public List<string> TypesProducts { get; set; }
        public List<Product> SuggestedProducts{ get; set; }
    }
}
